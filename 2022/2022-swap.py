import json

def swap(n, v):

    posOne = 0
    posTwo = 0

    for i in range(0, n):

        if (i - 1) >= 0:

            prevNum = i - 1

        else: 

            prevNum = i

        if (i + 1) < n:

            nextNum = i + 1

        else:

            nextNum = posOne

        if v[i] > v[nextNum]: 

            posOne = i

        else:

            if v[posOne] > v[prevNum] and v[posOne] <= v[nextNum]:  
                
                posTwo = i

                if posTwo == n - 1 and posOne == 0: 
                    
                    return 1

                break
    
    return posTwo - posOne

def testSwap():

    with open("./swap.json", "r") as s: data = json.load(s)

    for test in data["data"]: print(swap(test[0], test[1]))

testSwap()
