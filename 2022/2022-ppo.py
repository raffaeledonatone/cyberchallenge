import json

def ppo(newPW, oldPW):

    if len(set(newPW) - set(oldPW)) == 1 or len(set(oldPW) - set(newPW)) == 1: return 0

    else:
        
        if len(newPW) < 8 or len(newPW) > 16: return 0

        else:
            
            up = 0
            low = 0
            digit = 0
            special = 0

            for i in range(0, len(newPW)):

                char = newPW[i]

                if i > 0 and char == newPW[i - 1]: return 0

                if char in "0123456789": digit = 1
                if char in "ABCDEFGHIJKLMNOPQRSTUVWXYZ": up = 1
                if char in "abcdefghijklmnopqrstuvwxyz": low = 1
                if char in "!@#$%^&*()_+-=[]\;',./{}|:<>?": special = 1
            
            if not(up and low) or not(digit and special): return 0

            return 1
        
def testPpo():

    with open("./ppo.json", "r") as p: data = json.load(p)

    for pwPair in data["data"][1]: print(ppo(pwPair[0], pwPair[1]))

testPpo()
